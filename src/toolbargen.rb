# coding: utf-8

class ToolBarGen_box < Gtk::Toolbar

	def initialize window
	
		super()
		
    @window = window
    
    set_toolbar_style Gtk::Toolbar::Style::BOTH
    
    taille = 48
		
		opentb = Gtk::ToolButton.new( Gtk::Image.new( "./icons/open#{taille}.png" ), 'Ouvrir' )
		savetb = Gtk::ToolButton.new( Gtk::Image.new( "./icons/save#{taille}.png" ), 'Enregistrer' )
    abouttb = Gtk::ToolButton.new( Gtk::Image.new( "./icons/help-about#{taille}.png" ), 'A propos' )
    quittb = Gtk::ToolButton.new( Gtk::Image.new( "./icons/application-exit#{taille}.png" ), 'Quitter' )
	
		opentb.signal_connect( "clicked" ) { 
	  	dialog = Gtk::FileChooserDialog.new("Ouvrir le dossier de traduction",
						                               window,
						                               Gtk::FileChooser::ACTION_OPEN,
						                               nil,
						                               [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
						                               [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])

			dialog.set_action Gtk::FileChooser::ACTION_SELECT_FOLDER
			if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
				check dialog.filename
			end
			dialog.destroy
	  }
	  
	  savetb.signal_connect( "clicked" ) { 
			window.interface.save
		}
		
		abouttb.signal_connect( "clicked" ) { 
			about = About.new window.version
			about.signal_connect('response') { about.destroy }
		}
		  
	  quittb.signal_connect( "clicked" ) { 
	  	window.quit
	  }
        
    tool = [ opentb, savetb, abouttb, quittb]
    
    tool.each_index { |i| 
    	self.insert i, tool[i]
    }
	
	end
	
	def check folder
		yamls = []
		Dir.glob(File.join(folder, "*.yml")).sort.each { |f|
			yamls << File.basename(f) if File.extname(f).eql?(".yml")
		}
		
		if !yamls.empty?
			ref = choose_language yamls, true
			lang = choose_language yamls-[ref], false
			
			if (!ref.empty? and !lang.empty?)
				@window.interface.remplir_tree File.join(folder,ref), File.join(folder,lang)
			end
		end
		
	end
	
	def choose_language yamls, ref=true
		rep = ""
		dialog = ChooseLanguages.new @window, yamls, ref
		if dialog.run == Gtk::Dialog::RESPONSE_OK
			rep = (dialog.creation.text.empty? ? dialog.choix.active_text : dialog.creation.text )
		end
		dialog.destroy
		return rep
	end

end
