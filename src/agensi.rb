#!/usr/bin/env ruby
# coding: utf-8

require 'rubygems'
require 'bundler/setup' unless RUBY_PLATFORM.include?("mingw")
require 'gtk2'
require 'yaml'
require './toolbargen.rb'
require './about.rb'
require './interface.rb'
require './choose_languages.rb'

class TranslateWindow < Gtk::Window

	attr_reader :version, :interface

	def initialize level
		super
		
		signal_connect( "destroy" ) { Gtk.main_quit }
		
		GLib.set_application_name "Agensi"
		@version = "0.0.1"
		set_title GLib.application_name + " v" + @version
		set_icon( "./icons/icon.png" )	
		
		set_window_position Gtk::Window::POS_CENTER
		set_default_size 800, 300
		
		agencement
		maximize
	end
	
	def agencement
		@toolbargen = ToolBarGen_box.new self
		@interface = Interface.new self
		@vbox = Gtk::VBox.new false, 2
		@vbox.pack_start @toolbargen, false, false, 0
		@vbox.pack_start @interface, true, true, 0
		
		add @vbox
		
		show_all
	end
	
	def quit
		dialog = Gtk::MessageDialog.new(self, 
                                Gtk::Dialog::DESTROY_WITH_PARENT,
                                Gtk::MessageDialog::QUESTION,
                                Gtk::MessageDialog::BUTTONS_YES_NO,
                                "Voulez vous réellement quitter le programe ?")
		response = dialog.run
		case response
		  when Gtk::Dialog::RESPONSE_YES
				Gtk.main_quit
		end 
		dialog.destroy
	end

end

Gtk.init
win = TranslateWindow.new :toplevel
Gtk.main


