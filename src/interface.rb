# coding: utf-8

class Interface < Gtk::HBox

	def initialize window
		super(false,2)		
    @window = window
    @lang = nil
    agencement
	end
	
	def agencement
		self.pack_start tree, false, false, 3
		self.pack_start editeurs, true, true, 3
	end
	
	def tree
		@tree = Gtk::TreeView.new Gtk::TreeStore.new(String, String, Gdk::Pixbuf, String)
		@tree.signal_connect ("button-release-event") { |view, path, column|
			if @tree.selection.selected
				if @tree.selection.selected.parent
					@text_orig.buffer.text = @tree.model.get_value @tree.selection.selected, 1
					@text_trad.buffer.text = @tree.model.get_value @tree.selection.selected, 3
				else
					@text_orig.buffer.text = ""
					@text_trad.buffer.text = ""
				end
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		# Colonne pour la photo
		renderer_pix = Gtk::CellRendererPixbuf.new	
		renderer_pix.xalign = 0.5
		renderer_pix.yalign = 0
		
		col = Gtk::TreeViewColumn.new("", renderer_left, :text => 0)
		col.resizable = true
		col.expand = true
		@tree.append_column(col)
		
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 2)
		col.resizable = true
		@tree.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_NEVER,Gtk::POLICY_AUTOMATIC)
  	scroll.add @tree
  	
  	scroll
	end
	
	def editeurs
		vbox = Gtk::VBox.new true, 2
		vbox1 = Gtk::VBox.new false, 2
		vbox2 = Gtk::VBox.new false, 2
	
		vbox1.pack_start Gtk::Label.new("Texte original"), false, false, 3
		@text_orig = Gtk::TextView.new
		@text_orig.sensitive = false
		scroll1 = Gtk::ScrolledWindow.new
  	scroll1.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll1.add @text_orig
		vbox1.pack_start scroll1, true, true, 3
		vbox2.pack_start Gtk::Label.new("Traduction"), false, false, 3
		@text_trad = Gtk::TextView.new
		scroll2 = Gtk::ScrolledWindow.new
  	scroll2.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll2.add @text_trad
		vbox2.pack_start scroll2, true, true, 3 
		
		@text_trad.buffer.signal_connect ("changed") {
			if @tree.selection.selected
				if @tree.selection.selected.parent
					@tree.selection.selected[3] = @text_trad.buffer.text.to_s
					if @text_trad.buffer.text.to_s.empty?
						@tree.selection.selected[2] = @pix_vide
					else
						@tree.selection.selected[2] = @pix_plein
					end
				end
			end
		} 	
  	
		vbox.add vbox1
		vbox.add vbox2
		vbox
	end
	
	def remplir_tree ref, lang
		@tree.model.clear
		@lang = lang
		
		reference = YAML.load_file(ref)
		langue = YAML.load_file(lang)
		
		@pix_plein = Gdk::Pixbuf.new("icons/plein.png", 16, 16)
		@pix_vide = Gdk::Pixbuf.new("icons/vide.png", 16, 16)
		
		reference.each do |key, value|
			iter = @tree.model.append nil
			iter[0] = key
			iter[1] = ""
			value.each do |key2, value2|
				iter2 = @tree.model.append iter
				iter2[0] = key2
				iter2[1] = value2
				if langue[key]
					if langue[key][key2].to_s.empty?
						iter2[2] = @pix_vide
						iter2[3] = ""
					else
						iter2[2] = @pix_plein
						iter2[3] = langue[key][key2].to_s
					end
				else
					iter2[2] = @pix_vide
					iter2[3] = ""
				end
			end
		end
		@tree.expand_all
	end
	
	def save
		yml = {}
		parent = nil
		children = nil
		@tree.model.each { |model, path, iter|
			if iter.has_child?
				if parent
					yml.merge!({parent[0] => children}) unless children.empty?
				end
				parent = iter
				children = {}
			else
				children.merge!({iter[0] => iter[3]}) unless iter[3].empty?
			end
		}
		if parent
			yml.merge!({parent[0] => children}) unless children.empty?
		end
		
		if !yml.empty?
			File.open(@lang, 'w') do |out|
   			YAML.dump(yml, out)
			end

			dialog = Gtk::MessageDialog.new(@window, 
		                              Gtk::Dialog::DESTROY_WITH_PARENT,
		                              Gtk::MessageDialog::INFO,
		                              Gtk::MessageDialog::BUTTONS_OK,
		                              "Votre fichier de langue est bien sauvegardé")
			response = dialog.run
			dialog.destroy
		end
	end
	
end
