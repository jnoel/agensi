# coding: utf-8
		
class About < Gtk::AboutDialog

	def initialize version
		
		super()

		self.version = version
		self.comments = "Logiciel de traduction sur fichiers YAML"
		lic = ""
		file = File.open("../LICENCE", "r")
		while (line=file.gets)
			lic += line
		end
		file.close
		self.license = lic
		self.website = ""
		self.authors = ["Jean-Noël Rouchon"]
		self.artists = ["Faience icon theme"]
		self.logo = Gdk::Pixbuf.new( "./icons/icon.png" )
		self.set_icon( "./icons/icon.png" )
		
		self.show
	
	end

end		
