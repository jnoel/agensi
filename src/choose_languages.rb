# coding: utf-8

class ChooseLanguages < Gtk::Dialog

	attr_reader :choix, :creation
	
	def initialize window, langues, ref=true
		
		titre = (ref ? "Choix de la langue de référence" : "Choix de la langue à saisir")
		super(titre, window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		#set_default_size 900, 300
		
		@choix = Gtk::ComboBox.new
		@creation = Gtk::Entry.new
		
		if !langues.empty?
			hbox = Gtk::HBox.new false, 2
			hbox.add Gtk::Label.new (ref ? "Choisissez la langue de référence : " : "Choisissez la langue à saisir : " )
			hbox.add @choix
		
			self.vbox.pack_start hbox, false, false, 3
		end
		
		if false
			hbox2 = Gtk::HBox.new false, 2
			hbox2.add Gtk::Label.new "Créez un nouvelle langue : "
			hbox2.add @creation
		
			self.vbox.pack_start hbox2, false, false, 3
		end
		
		self.vbox.show_all
		
		remplir_choix langues
	
	end
	
	def remplir_choix langues
		langues.each do |langue|	
			@choix.append_text langue
		end
		@choix.active = 0
	end

end		

