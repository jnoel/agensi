#!/usr/bin/env ruby
# coding: utf-8

f = File.new('agensi.sh', 'w')
f.write("#!/bin/bash\n")
f.write("cd #{File.join(Dir.getwd,'src')}\n")
f.write("ruby agensi.rb\n")
f.chmod(0770)
f.close

f = File.new(File.join(Dir.home,'.local/share/applications/agensi.desktop'), 'w')
f.write("#!/usr/bin/env xdg-open\n\n")
f.write("[Desktop Entry]\n")
f.write("Encoding=UTF-8\n")
f.write("Name=Agensi\n")
f.write("Name[fr]=Agensi\n")
f.write("Name[fr_FR]=Agensi\n")
f.write("Exec=#{File.join(Dir.getwd,'agensi.sh')}\n")
f.write("Icon=#{File.join(Dir.getwd,'src/icons/icon.png')}\n")
f.write("Terminal=false\n")
f.write("Type=Application\n")
f.write("StartupNotify=true\n")
f.write("Categories=Office\n")
f.chmod(0770)
f.close
